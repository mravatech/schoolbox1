<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\StudentParent;
use Hash;

class ParentExists implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    
    public function __construct($request)
    {
        $this->login = $request->login;
        $this->password = $request->password;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $parent_exists = StudentParent::where('parentID' , $value)
                                ->orWhere('phone', $value)
                                ->first();
        return $parent_exists !== NULL AND Hash::check($this->password, optional($parent_exists)->password);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if (starts_with($this->login, 'pa'))
        {
            return 'The parent ID is invalid or Wrong password';
        }
        return 'The phone number does not exist or Wrong password';
    }
}
