<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Admin;
use Hash;

class AdminExists implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    
    public function __construct($request)
    {
        $this->login = $request->login;
        $this->password = $request->password;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // dd( bcrypt(request()->password));
        $admin_exists = Admin::where('adminID', $value)
                                ->orWhere('phone', $value)
                                ->first();
        return $admin_exists !== NULL AND Hash::check($this->password, optional($admin_exists)->password);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if (starts_with($this->login, 'ad'))
        {
            return 'The admin ID is invalid or Wrong password';
        }
        return 'The phone number does not exist or Wrong password';
    }
}
