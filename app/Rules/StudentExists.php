<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Student;
use Hash;

class StudentExists implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    
    public function __construct($request)
    {
        $this->login = $request->login;
        $this->password = $request->password;

        // dd($request->all());
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $student_exists = Student::where('studentID' , $value)
                                ->orWhere('phone', $value)
                                ->first();

        return $student_exists !== NULL AND Hash::check($this->password, optional($student_exists)->password);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if (starts_with($this->login, 'st'))
        {
            return 'The Student ID is invalid or Wrong password';
        }
        return 'The phone number does not exist or Wrong password';
    }
}
