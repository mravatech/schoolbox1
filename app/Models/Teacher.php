<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    public function class()
    {
    	return $this->hasOne('\App\Models\Class', 'teacher_id');
    }
}
