<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    public function terms()
    {
    	return $this->hasMany('\App\Models\Term', 'session_id');
    }
}
