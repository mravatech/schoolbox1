<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class StudentParent extends Authenticatable
{
	use Notifiable;
	protected $guard = 'parent';
    protected $table = 'parents';
    public function students()
    {
    	return $this->hasMany('\App\Models\Student', 'student_id');
    }
    
    public function setPasswordAttribute($value)
    {
    	$this->attributes['password'] = bcrypt($value);
    }

}
