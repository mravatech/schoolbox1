<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Class extends Model
{
    public function students()
    {
    	return $this->hasMnay('\App\Models\Class', 'class_id');
    }

     public function teacher()
    {
    	return $this->belongsTo('\App\Models\Teacher', 'teacher_id');
    }
}
