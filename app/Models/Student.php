<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Student extends Authenticatable
{
	use Notifiable; 

	protected $guard = 'student';

    public function department()
    {
    	return $this->belongsTo('\App\Models\Department', 'department_id');
    }

    public function parent()
    {
    	return $this->belongsTo('\App\Models\StudentParent', 'student_id');
    }

    public function setPasswordAttribute($value)
    {
    	$this->attributes['password'] = bcrypt($value);
    }

}
