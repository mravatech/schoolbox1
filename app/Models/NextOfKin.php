<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NextOfKin extends Model
{
    public function students()
    {
    	return $this->hasMany('\App\Models\Student', 'next_of_kin_id');
    }
}
