<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    public function session()
    {
    	return $this->belongsTo('\App\Models\Session', 'session_id');
    }
}
