<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Rules\AdminExists;

class AdminLoginController extends Controller{

    public function __construct(){

        $this->middleware('guest:admin', ['except'=>['logout']]);
    }

    public function showLoginForm(){

        return view('admin.login');
    }

    public function login(Request $request){

         $this->validate($request, [

            'password'=>'bail|required',
            'login' => ['required', new AdminExists($request) ]

        ]);
        
        if (Auth::guard('admin')->attempt($this->credentials(), $request->remember))
            {
            return redirect()->intended(route('admin.dashboard'));
        }

        return redirect()->back()->withInput($request->only('login', 'remember'));
    }

    protected function credentials()
    {
        $field = strtolower(request()->input('login'));

        if (starts_with($field, 'ad')) {

             return ['adminID' => request()->input('login'), 'password' => request()->input('password')];
        }
        else {
            return ['phone' => request()->input('login'), 'password' => request()->input('password')];
        }
       
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    

    public function logout(Request $request){
        Auth::guard('admin')->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

}
