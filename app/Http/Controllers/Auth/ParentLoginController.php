<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Rules\ParentExists;

class ParentLoginController extends Controller{

    public function __construct(){

        $this->middleware('guest:parent', ['except'=>['logout']]);
    }

    public function showLoginForm(){

        return view('parent.login');
    }

    public function login(Request $request){

        
        $this->validate($request, [

            'password'=>'bail|required',

            'login' => ['required', new ParentExists($request)],
        ]);

        if (Auth::guard('parent')->attempt($this->credentials(), $request->remember))
            {
            return redirect()->intended(route('parent.dashboard'));
        }

    return redirect()->back()->withInput($request->only('login', 'remember'));
    }

    protected function credentials()
    {
        $field = strtolower(request()->input('login'));

        if (starts_with($field, 'pa')) {

             return ['parentID' => request()->input('login'), 'password' => request()->input('password')];
        }
        else {
            return ['phone' => request()->input('login'), 'password' => request()->input('password')];
        }
       
    }

    public function logout(Request $request){
        Auth::guard('parent')->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

}
