<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Rules\StudentExists;

class StudentLoginController extends Controller{

    public function __construct(){

        $this->middleware('guest:student', ['except'=>['logout']]);
    }

    public function showLoginForm(){

        return view('student.login');
    }

    public function login(Request $request){

        $this->validate($request, [
            'password'=>'required',
            'login' => ['required', new StudentExists($request)]

        ]);

        // dd($this->credentials());
        if (Auth::guard('student')->attempt($this->credentials(), $request->remember)) {

            return redirect()->intended(route('student.dashboard'));
        }

    return redirect()->back()->withInput($request->only('login', 'remember'));
    }
    
    protected function credentials()
    {
       $field = strtolower(request()->input('login'));
        if (starts_with($field, 'st')) {

             return ['studentID' => request()->input('login'), 'password' => request()->input('password')];
        }
        else {
            return ['phone' => request()->input('login'), 'password' => request()->input('password')];
        }
       
    }

    public function logout(Request $request){
        Auth::guard('student')->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

}
