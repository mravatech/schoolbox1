<?php

namespace App\Http\Controllers\Parent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParentController extends Controller
{
     
    public function __construct()
    {
        $this->middleware('auth:parent');
    }

    public function index()
    {   
        return view('parent.index');
    }
}
