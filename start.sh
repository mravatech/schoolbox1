#!/bin/bash

if [ ! -z $1 ]
then 
	docker exec -it $1 bash
	exit 0
fi

read -p "To start the containers enter Y, to stop the containers enter N: " response


if [ $response == 'Y' ] || [ $response == 'y' ]

then 
	echo 'Starting the containers...'

	docker-compose -f docker-compose-dev.yaml up -d
elif [ $response == 'N' ] || [ $response == 'n' ]
then
	echo "stopping the containers..."

	docker kill sb.core sb.mysql sb.pma
fi

if [ ! -z  $1 ] 

then
	docker exec -it $1 bash

fi
