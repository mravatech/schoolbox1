<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/add-student', 'StudentController@AddStudent')->name('AddStudent');


////////=======================Admin Routes================================///>

Route::group(['prefix' => 'admin'], function(){
	  Route::get('/', 'Admin\AdminController@index')->name('admin.dashboard');
	  Route::get('login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	  Route::post('login', 'Auth\AdminLoginController@login')->name('admin');
	  Route::post('logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

});

////////=======================End Admin Routes================================///>




////////=======================Student Routes================================///>

Route::group(['prefix' => 'student'], function(){
	  Route::get('/', 'Student\StudentController@index')->name('student.dashboard');
	  Route::get('login', 'Auth\StudentLoginController@showLoginForm')->name('student.login');
	  Route::post('login', 'Auth\StudentLoginController@login')->name('student');
	  Route::post('logout', 'Auth\StudentLoginController@logout')->name('student.logout');

});

////////=======================End Student Routes================================///>




////////=======================Parent Routes================================///>

Route::group(['prefix' => 'parent'], function(){
	  Route::get('/', 'Parent\ParentController@index')->name('parent.dashboard');
	  Route::get('login', 'Auth\ParentLoginController@showLoginForm')->name('parent.login');
	  Route::post('login', 'Auth\ParentLoginController@login')->name('parent');
	  Route::post('logout', 'Auth\ParentLoginController@logout')->name('parent.logout');

});

////////=======================End Parent Routes================================///>


