<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('adminID')->unique();
            $table->string('fname');
            $table->string('lname');
            $table->string('title')->nullable();
            $table->string('oname')->nullable(); //other names
            $table->string('email')->unique()->nullable();
            $table->string('phone')->unique();
            $table->string('password');
            $table->string('picture_url')->nullable();
            $table->string('role_id')->nullable();
            $table->string('class_id')->nullable(); //if the admin is a class teacher
            $table->enum('sex', ['Male', 'Female'])->nullable();
            $table->string('dob')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('admins')->insert([
            'fname' => 'Super',
            'lname' => 'Admin',
            'password' => bcrypt('admin'),
            'adminID' => 'ad1234',
            'phone' => '07035052689'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
