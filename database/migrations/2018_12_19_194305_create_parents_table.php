<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('parentID')->unique();
            $table->string('fname');
            $table->string('lname');
            $table->string('oname')->nullable();
            $table->string('password')->nullable();
            $table->string('phone')->unique();
            $table->string('email')->unique()->nullable();
            $table->string('address')->nullable();
            $table->string('picture_url')->nullable();
            $table->string('lga_id')->nullable();
            $table->string('state_id')->nullable();
            $table->string('nationlality')->nullable();
            $table->string('occupation')->nullable();
            $table->string('religion')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

           DB::table('parents')->insert([
            'parentID' => 'pa1234',
            'fname'    => 'Abdul',
            'lname' => 'Shafi',
            'password' => bcrypt('parent'),
            'phone' => '08027664440'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parents');
    }
}
