<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('studentID')->unique();
            $table->string('fname');
            $table->string('lname');
            $table->string('password');
            $table->string('oname')->nullable();
            $table->string('phone')->nullable()->unique();
            $table->string('dob')->nullable();
            $table->enum('sex', ['Male', 'Female'])->nullable();
            $table->unsignedInteger('session_id')->nullable();
            $table->unsignedInteger('class_id')->nullable();
            $table->unsignedInteger('department_id')->nullable();
            $table->unsignedInteger('next_of_kin_id')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('address')->nullable();
            $table->string('parent_id')->nullable();
            $table->string('lga_id')->nullable();
            $table->string('state_id')->nullable();
            $table->string('nationlality')->nullable();
            $table->string('occupation')->nullable();
            $table->string('religion')->nullable();
            $table->string('picture_url')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('students')->insert([
            'studentID' => 'st1234',
            'fname'    => 'Abdul',
            'lname' => 'Shafi',
            'password' => bcrypt('student'),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
