<div class="main-menu menu-fixed menu-light menu-accordion    menu-shadow " data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" navigation-header">
                <span data-i18n="nav.category.layouts">Management</span><i class="la la-ellipsis-h ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Management"></i>
            </li>


            <li class=" nav-item"><a href="#"><i class="la la-graduation-cap"></i><span class="menu-title" data-i18n="nav.color_palette.main">Student</span></a>
                <ul class="menu-content">
                    <li>
                      <a class="menu-item" href="color-palette-primary.html" data-i18n="nav.color_palette.color_palette_primary">Student List</a>
                    </li>
                    <li>
                      <a class="menu-item" href="{{ route('AddStudent') }}" data-i18n="nav.color_palette.color_palette_danger">Add Student</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item"><a href="#"><i class="la la-book"></i><span class="menu-title" data-i18n="nav.starter_kit.main">Result</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-1-column.html" data-i18n="nav.starter_kit.1_column">1 column</a>
                    </li>
                    <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-2-columns.html" data-i18n="nav.starter_kit.2_columns">2 columns</a>
                    </li>
                    <li><a class="menu-item" href="#" data-i18n="nav.starter_kit.3_columns_detached.main">Content Det. Sidebar</a>
                        <ul class="menu-content">
                            <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-content-detached-left-sidebar.html" data-i18n="nav.starter_kit.3_columns_detached.3_columns_detached_left_sidebar">Detached left sidebar</a>
                            </li>
                            <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-content-detached-left-sticky-sidebar.html" data-i18n="nav.starter_kit.3_columns_detached.3_columns_detached_sticky_left_sidebar">Detached sticky left sidebar</a>
                            </li>
                            <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-content-detached-right-sidebar.html" data-i18n="nav.starter_kit.3_columns_detached.3_columns_detached_right_sidebar">Detached right sidebar</a>
                            </li>
                            <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-content-detached-right-sticky-sidebar.html" data-i18n="nav.starter_kit.3_columns_detached.3_columns_detached_sticky_right_sidebar">Detached sticky right sidebar</a>
                            </li>
                        </ul>
                    </li>
                    <li class="navigation-divider"></li>
                    <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-fixed-navbar.html" data-i18n="nav.starter_kit.fixed_navbar">Fixed navbar</a>
                    </li>
                    <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-fixed-navigation.html" data-i18n="nav.starter_kit.fixed_navigation">Fixed navigation</a>
                    </li>
                    <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-fixed-navbar-navigation.html" data-i18n="nav.starter_kit.fixed_navbar_navigation">Fixed navbar &amp; navigation</a>
                    </li>
                    <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-fixed-navbar-footer.html" data-i18n="nav.starter_kit.fixed_navbar_footer">Fixed navbar &amp; footer</a>
                    </li>
                    <li class="navigation-divider"></li>
                    <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-fixed.html" data-i18n="nav.starter_kit.fixed_layout">Fixed layout</a>
                    </li>
                    <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-boxed.html" data-i18n="nav.starter_kit.boxed_layout">Boxed layout</a>
                    </li>
                    <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-static.html" data-i18n="nav.starter_kit.static_layout">Static layout</a>
                    </li>
                    <li class="navigation-divider"></li>
                    <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-light.html" data-i18n="nav.starter_kit.light_layout">Light layout</a>
                    </li>
                    <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-dark.html" data-i18n="nav.starter_kit.dark_layout">Dark layout</a>
                    </li>
                    <li><a class="menu-item" href="https://pixinvent.com/modern-admin-clean-bootstrap-4-dashboard-html-template/starter-kit/ltr/vertical-menu-template/layout-semi-dark.html" data-i18n="nav.starter_kit.semi_dark_layout">Semi dark layout</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item"><a href="changelog.html"><i class="la la-user-secret"></i><span class="menu-title" data-i18n="nav.changelog.main">Teacher</span></a>
              <ul class="menu-content">
                  <li><a class="menu-item" href="color-palette-primary.html" data-i18n="nav.color_palette.color_palette_primary">Primary palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-danger.html" data-i18n="nav.color_palette.color_palette_danger">Danger palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-success.html" data-i18n="nav.color_palette.color_palette_success">Success palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-warning.html" data-i18n="nav.color_palette.color_palette_warning">Warning palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-info.html" data-i18n="nav.color_palette.color_palette_info">Info palette</a>
                  </li>
                  <li class="navigation-divider"></li>
                  <li><a class="menu-item" href="color-palette-red.html" data-i18n="nav.color_palette.color_palette_red">Red palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-pink.html" data-i18n="nav.color_palette.color_palette_pink">Pink palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-purple.html" data-i18n="nav.color_palette.color_palette_purple">Purple palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-blue.html" data-i18n="nav.color_palette.color_palette_blue">Blue palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-cyan.html" data-i18n="nav.color_palette.color_palette_cyan">Cyan palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-teal.html" data-i18n="nav.color_palette.color_palette_teal">Teal palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-yellow.html" data-i18n="nav.color_palette.color_palette_yellow">Yellow palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-amber.html" data-i18n="nav.color_palette.color_palette_amber">Amber palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-blue-grey.html" data-i18n="nav.color_palette.color_palette_blue_grey">Blue Grey palette</a>
                  </li>
              </ul>
            </li>
            <li class=" nav-item"><a href="#"><i class="la la-money"></i><span class="menu-title" data-i18n="nav.disabled_menu.main">Accountant</span></a>
              <ul class="menu-content">
                  <li><a class="menu-item" href="color-palette-primary.html" data-i18n="nav.color_palette.color_palette_primary">Primary palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-danger.html" data-i18n="nav.color_palette.color_palette_danger">Danger palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-success.html" data-i18n="nav.color_palette.color_palette_success">Success palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-warning.html" data-i18n="nav.color_palette.color_palette_warning">Warning palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-info.html" data-i18n="nav.color_palette.color_palette_info">Info palette</a>
                  </li>
                  <li class="navigation-divider"></li>
                  <li><a class="menu-item" href="color-palette-red.html" data-i18n="nav.color_palette.color_palette_red">Red palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-pink.html" data-i18n="nav.color_palette.color_palette_pink">Pink palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-purple.html" data-i18n="nav.color_palette.color_palette_purple">Purple palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-blue.html" data-i18n="nav.color_palette.color_palette_blue">Blue palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-cyan.html" data-i18n="nav.color_palette.color_palette_cyan">Cyan palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-teal.html" data-i18n="nav.color_palette.color_palette_teal">Teal palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-yellow.html" data-i18n="nav.color_palette.color_palette_yellow">Yellow palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-amber.html" data-i18n="nav.color_palette.color_palette_amber">Amber palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-blue-grey.html" data-i18n="nav.color_palette.color_palette_blue_grey">Blue Grey palette</a>
                  </li>
              </ul>
            </li>
            <li class=" nav-item"><a href="#"><i class="la la-bank"></i><span class="menu-title" data-i18n="nav.disabled_menu.main">Liberian</span></a>
              <ul class="menu-content">
                  <li><a class="menu-item" href="color-palette-primary.html" data-i18n="nav.color_palette.color_palette_primary">Primary palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-danger.html" data-i18n="nav.color_palette.color_palette_danger">Danger palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-success.html" data-i18n="nav.color_palette.color_palette_success">Success palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-warning.html" data-i18n="nav.color_palette.color_palette_warning">Warning palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-info.html" data-i18n="nav.color_palette.color_palette_info">Info palette</a>
                  </li>
                  <li class="navigation-divider"></li>
                  <li><a class="menu-item" href="color-palette-red.html" data-i18n="nav.color_palette.color_palette_red">Red palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-pink.html" data-i18n="nav.color_palette.color_palette_pink">Pink palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-purple.html" data-i18n="nav.color_palette.color_palette_purple">Purple palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-blue.html" data-i18n="nav.color_palette.color_palette_blue">Blue palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-cyan.html" data-i18n="nav.color_palette.color_palette_cyan">Cyan palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-teal.html" data-i18n="nav.color_palette.color_palette_teal">Teal palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-yellow.html" data-i18n="nav.color_palette.color_palette_yellow">Yellow palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-amber.html" data-i18n="nav.color_palette.color_palette_amber">Amber palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-blue-grey.html" data-i18n="nav.color_palette.color_palette_blue_grey">Blue Grey palette</a>
                  </li>
              </ul>
            </li>
            <li class=" nav-item"><a href="#"><i class="la la-user-secret"></i><span class="menu-title" data-i18n="nav.disabled_menu.main">Admin</span></a>
              <ul class="menu-content">
                  <li><a class="menu-item" href="color-palette-primary.html" data-i18n="nav.color_palette.color_palette_primary">Primary palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-danger.html" data-i18n="nav.color_palette.color_palette_danger">Danger palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-success.html" data-i18n="nav.color_palette.color_palette_success">Success palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-warning.html" data-i18n="nav.color_palette.color_palette_warning">Warning palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-info.html" data-i18n="nav.color_palette.color_palette_info">Info palette</a>
                  </li>
                  <li class="navigation-divider"></li>
                  <li><a class="menu-item" href="color-palette-red.html" data-i18n="nav.color_palette.color_palette_red">Red palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-pink.html" data-i18n="nav.color_palette.color_palette_pink">Pink palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-purple.html" data-i18n="nav.color_palette.color_palette_purple">Purple palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-blue.html" data-i18n="nav.color_palette.color_palette_blue">Blue palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-cyan.html" data-i18n="nav.color_palette.color_palette_cyan">Cyan palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-teal.html" data-i18n="nav.color_palette.color_palette_teal">Teal palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-yellow.html" data-i18n="nav.color_palette.color_palette_yellow">Yellow palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-amber.html" data-i18n="nav.color_palette.color_palette_amber">Amber palette</a>
                  </li>
                  <li><a class="menu-item" href="color-palette-blue-grey.html" data-i18n="nav.color_palette.color_palette_blue_grey">Blue Grey palette</a>
                  </li>
              </ul>
            </li>



            <li class=" navigation-header">
                <span data-i18n="nav.category.layouts">Communication</span><i class="la la-ellipsis-h ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Communication"></i>
            </li>
            <li class=" nav-item"><a href="email-application.html"><i class="la la-envelope"></i><span class="menu-title" data-i18n="">Email </span></a>
            </li>
            <li class=" nav-item"><a href="chat-application.html"><i class="la la-comments"></i><span class="menu-title" data-i18n="">Chat </span></a>
            </li>

            <li class=" navigation-header">
                <span data-i18n="nav.category.pages">School Settings</span><i class="la la-ellipsis-h ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="School Settings"></i>
            </li>

            <li class=" nav-item"><a href="#"><i class="la la-briefcase"></i><span class="menu-title" data-i18n="nav.project.main">School Profile</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="project-summary.html" data-i18n="nav.project.project_summary">Project Summary</a>
                    </li>
                    <li><a class="menu-item" href="project-tasks.html" data-i18n="nav.project.project_tasks">Project Task</a>
                    </li>
                    <li><a class="menu-item" href="project-bugs.html" data-i18n="nav.project.project_bugs">Project Bugs</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item"><a href="scrumboard.html"><i class="la la-check-square"></i><span class="menu-title" data-i18n="nav.scrumboard.main">School Calendar</span><span class="badge badge badge-info float-right">Update</span></a>
            </li>
            <li class=" nav-item"><a href="scrumboard.html"><i class="la la-clipboard"></i><span class="menu-title" data-i18n="nav.scrumboard.main">Notice Board</span><span class="badge badge badge-info float-right">Update</span></a>
            </li>
            <li class=" nav-item"><a href="scrumboard.html"><i class="la la-calendar"></i><span class="menu-title" data-i18n="nav.scrumboard.main">Event</span><span class="badge badge badge-info float-right">Update</span></a>
            </li>
            <li class=" navigation-header">
                <span data-i18n="nav.category.pages">Class Settings</span><i class="la la-ellipsis-h ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Class Settings"></i>
            </li>

            <li class=" nav-item"><a href="#"><i class="la la-clipboard"></i><span class="menu-title" data-i18n="nav.invoice.main">Class Allocation </span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="invoice-summary.html" data-i18n="nav.invoice.invoice_summary">Invoice Summary</a>
                    </li>
                    <li><a class="menu-item" href="invoice-template.html" data-i18n="nav.invoice.invoice_template">Invoice Template</a>
                    </li>
                    <li><a class="menu-item" href="invoice-list.html" data-i18n="nav.invoice.invoice_list">Invoice List</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item"><a href="#"><i class="la la-film"></i><span class="menu-title" data-i18n="nav.timelines.main">Timetable</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="timeline-center.html" data-i18n="nav.timelines.timeline_center">Timelines Center</a>
                    </li>
                    <li><a class="menu-item" href="timeline-left.html" data-i18n="nav.timelines.timeline_left">Timelines Left</a>
                    </li>
                    <li><a class="menu-item" href="timeline-right.html" data-i18n="nav.timelines.timeline_right">Timelines Right</a>
                    </li>
                    <li><a class="menu-item" href="timeline-horizontal.html" data-i18n="nav.timelines.timeline_horizontal">Timelines Horizontal</a>
                    </li>
                </ul>
            </li>
            <li class=" nav-item"><a href="#"><i class="la la-calendar-check-o"></i><span class="menu-title" data-i18n="nav.timelines.main">Attendance</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="timeline-center.html" data-i18n="nav.timelines.timeline_center">Timelines Center</a>
                    </li>
                    <li><a class="menu-item" href="timeline-left.html" data-i18n="nav.timelines.timeline_left">Timelines Left</a>
                    </li>
                    <li><a class="menu-item" href="timeline-right.html" data-i18n="nav.timelines.timeline_right">Timelines Right</a>
                    </li>
                    <li><a class="menu-item" href="timeline-horizontal.html" data-i18n="nav.timelines.timeline_horizontal">Timelines Horizontal</a>
                    </li>
                </ul>
            </li>

            <li class=" navigation-header">
                <span data-i18n="nav.category.pages">Media</span><i class="la la-ellipsis-h ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Settings"></i>
            </li>

            <li class=" navigation-header">
                <span data-i18n="nav.category.pages">Settings</span><i class="la la-ellipsis-h ft-minus" data-toggle="tooltip" data-placement="right" data-original-title="Settings"></i>
            </li>

            <li class=" nav-item"><a href="#"><i class="la la-user"></i><span class="menu-title" data-i18n="nav.users.main">Profile</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="user-profile.html" data-i18n="nav.users.user_profile">Users Profile</a>
                    </li>
                    <li><a class="menu-item" href="user-cards.html" data-i18n="nav.users.user_cards">Users Cards</a>
                    </li>
                    <li><a class="menu-item" href="users-contacts.html" data-i18n="nav.users.users_contacts">Users List</a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
</div>
