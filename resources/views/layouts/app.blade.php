<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Quicksand:300,400,500,700"
    rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"
    rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('style/vendors.min.css')}}">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('style/app.min.css')}}">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('style/vertical-menu.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('style/palette-gradient.min.css')}}">
    {{-- <link rel="stylesheet" type="text/css" href="{{asset('style/cryptocoins.css"> --}}
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">


    <!-- END Custom CSS-->
</head>
@guest

<body class="vertical-layout vertical-menu 1-column  bg-full-screen-image menu-expanded blank-page blank-page"
data-open="click" data-menu="vertical-menu" data-col="1-column">
@else

<body class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar"
  data-open="click" data-menu="vertical-menu" data-col="2-columns">
@endguest
  {{-- <div id="loader">
      <div class="spinner"></div>
  </div> --}}
  {{-- <script type="text/javascript">
      window.addEventListener('load', () => {
              const loader = document.getElementById('loader');
              setTimeout(() => {
                loader.classList.add('fadeOut');
              }, 300);
            });
  </script> --}}
    <div id="app">
        <main class="">
            @yield('content')
        </main>
    </div>
    @guest

    @else

    <footer class="footer footer-static footer-light navbar-border navbar-shadow">
      <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2">
        <span class="float-md-left d-block d-md-inline-block">Copyright &copy; 2018 <a class="text-bold-800 grey darken-2" href="https://themeforest.net/user/pixinvent/portfolio?ref=pixinvent"
          target="_blank">Avatech </a>, All rights reserved. </span>
        <span class="float-md-right d-block d-md-inline-blockd-none d-lg-block">Hand-crafted & Made with <i class="ft-heart pink"></i></span>
      </p>
    </footer>
  @endguest

    <!-- BEGIN VENDOR JS-->
    <script src="{{asset('style/vendors.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('style/app-menu.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('style/app.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('style/customizer.min.js')}}" type="text/javascript"></script>
    <!-- END MODERN JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="{{asset('style/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('style/fullcalendar.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('style/calendar/fullcalendar.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/custom.js" type="text/javascript')}}"></script>

    <!-- END PAGE LEVEL JS-->
    <!-- End custom js for this page-->

</body>
</html>
