@extends('layouts.app')
@extends('layouts.header')
@extends('layouts.nav')
@section('content')

<link rel="stylesheet" type="text/css" href="style/calendar/fullcalendar.min.css">
<link rel="stylesheet" type="text/css" href="style/fullcalendar.min.css">

<link rel="stylesheet" type="text/css" href="style/palette-gradient.min.css">

<link rel="stylesheet" type="text/css" href="style/simple-line-icons/style.min.css">

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            {{-- --}}
            <!-- Full calendar events example section start -->
            <div class="row">
              <div class="col-xl-3 col-lg-6 col-12">
                <div class="card pull-up">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="media d-flex">
                        <div class="media-body text-left">
                          <h3 class="info">850</h3>
                          <h6>Student</h6>
                        </div>
                        <div>
                          <i class="icon-graduation info font-large-2 float-right"></i>
                        </div>
                      </div>
                      <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                        <div class="progress-bar bg-gradient-x-info" role="progressbar" style="width: 80%"
                        aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <div class="card pull-up">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="media d-flex">
                        <div class="media-body text-left">
                          <h3 class="warning">748</h3>
                          <h6>Parent</h6>
                        </div>
                        <div>
                          <i class="icon-users warning font-large-2 float-right"></i>
                        </div>
                      </div>
                      <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                        <div class="progress-bar bg-gradient-x-warning" role="progressbar" style="width: 65%"
                        aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <div class="card pull-up">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="media d-flex">
                        <div class="media-body text-left">
                          <h3 class="success">146</h3>
                          <h6>New Customers</h6>
                        </div>
                        <div>
                          <i class="icon-user-following success font-large-2 float-right"></i>
                        </div>
                      </div>
                      <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                        <div class="progress-bar bg-gradient-x-success" role="progressbar" style="width: 75%"
                        aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 col-12">
                <div class="card pull-up">
                  <div class="card-content">
                    <div class="card-body">
                      <div class="media d-flex">
                        <div class="media-body text-left">
                          <h3 class="danger">10</h3>
                          <h6>New Student</h6>
                        </div>
                        <div>
                          <i class="icon-user-follow danger font-large-2 float-right"></i>
                        </div>
                      </div>
                      <div class="progress progress-sm mt-1 mb-0 box-shadow-2">
                        <div class="progress-bar bg-gradient-x-danger" role="progressbar" style="width: 85%"
                        aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <div class="row">
                <div class="col-xl-6 col-12">
                    <section id="events-examples">
                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h4 class="card-title">Events</h4>
                                        <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                        <div class="heading-elements">
                                            <ul class="list-inline mb-0">
                                                <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                                <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                                <li><a data-action="close"><i class="ft-x"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body">
                                            <div id='fc-bg-events'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- News -->
                <div class="col-xl-6 col-12">

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">News</h4>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <td>
                                            <button class="btn btn-sm round btn-success btn-glow"><i class="la la-mark font-medium-1"></i> View All</button>
                                        </td>
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="table-responsive">
                                        <table class="table table-de mb-0">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Type</th>
                                                    <th>Title</th>
                                                    <th>Content</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>2018-01-31 06:51:51</td>
                                                    <td class="success">Genral</td>
                                                    <td><i class="cc BTC-alt"></i> 0.58647</td>
                                                    <td><i class="cc BTC-alt"></i> 0.58647</td>
                                                    <td>
                                                        <button class="btn btn-sm round btn-outline-success"> View</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2018-01-31 06:50:50</td>
                                                    <td class="cyan">Student</td>
                                                    <td><i class="cc BTC-alt"></i> 1.38647</td>
                                                    <td><i class="cc BTC-alt"></i> 0.38647</td>
                                                    <td>
                                                        <button class="btn btn-sm round btn-outline-success"> View</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2018-01-31 06:49:51</td>
                                                    <td class="blue">Admin</td>
                                                    <td><i class="cc BTC-alt"></i> 0.45879</td>
                                                    <td><i class="cc BTC-alt"></i> 0.45879</td>
                                                    <td>
                                                        <button class="btn btn-sm round btn-outline-success"> View</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2018-01-31 06:51:51</td>
                                                    <td class="teal">Parent</td>
                                                    <td><i class="cc BTC-alt"></i> 0.89877</td>
                                                    <td><i class="cc BTC-alt"></i> 0.89877</td>
                                                    <td>
                                                        <button class="btn btn-sm round btn-outline-success"> View</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2018-01-31 06:51:51</td>
                                                    <td class="cyan">Student</td>
                                                    <td><i class="cc BTC-alt"></i> 0.45712</td>
                                                    <td><i class="cc BTC-alt"></i> 0.45712</td>
                                                    <td>
                                                        <button class="btn btn-sm round btn-outline-success"> View</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>2018-01-31 06:51:51</td>
                                                    <td class="blue-grey">Teacher</td>
                                                    <td><i class="cc BTC-alt"></i> 0.58647</td>
                                                    <td><i class="cc BTC-alt"></i> 0.58647</td>
                                                    <td>
                                                        <button class="btn btn-sm round btn-outline-success"> View</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- News -->
                </div>
                <!-- // Full calendar events example section end -->
                {{-- --}}

            </div>
        </div>
    </div>

    {{--
    <script src="style/moment.min.js" type="text/javascript"></script>
    <script src="style/fullcalendar.min.js" type="text/javascript"></script>
    <script src="style/calendar/fullcalendar.min.js" type="text/javascript"></script> --}}
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    @endsection @extends('layouts.footer')
