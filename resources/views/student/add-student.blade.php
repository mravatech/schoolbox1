@extends('layouts.app')
@extends('layouts.header')
@extends('layouts.nav')
<link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"rel="stylesheet">

@section('content')

<div class="app-content content">
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body">
            <div class="col-xl-6 col-lg-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-block">
                        <div class="card-body">
                          <div>
                            <label class="" for="iconLeft4">Surname</label>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" name="surname" class="form-control" id="iconLeft4" placeholder="Surname">
                                <div class="form-control-position">
                                    <i class="la la-user font-medium-4" style="font-size: 2.4rem !important;"></i>
                                </div>
                            </fieldset>
                          </div>
                          <div>
                            <label class="" for="iconLeft4">First Name</label>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" name="first_name" class="form-control" id="iconLeft4" placeholder="First Name">
                                <div class="form-control-position">
                                    <i class="la la-user font-medium-4" style="font-size: 2.4rem !important;"></i>
                                </div>
                            </fieldset>
                          </div>

                          <div>
                            <label class="" for="iconLeft4">Other Name</label>
                            <fieldset class="form-group position-relative has-icon-left">
                                <input type="text" name="other_name" class="form-control" id="iconLeft4" placeholder="Other Name">
                                <div class="form-control-position">
                                    <i class="la la-user font-medium-4" style="font-size: 2.4rem !important;"></i>
                                </div>
                            </fieldset>
                          </div>

                          <fieldset>
                            <input type="radio" name="input-radio-3" id="input-radio-11">
                            <label for="input-radio-11">Radio Button</label>
                          </fieldset>
                          <fieldset>
                            <input type="radio" name="input-radio-3" id="input-radio-12" checked>
                            <label for="input-radio-12">Radio Button Checked</label>
                          </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@extends('layouts.footer')
